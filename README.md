An API made for the so-called "masters" app.

The end user needs to guess what's the game of origin of given screenshot/music track/quote (he chooses the category himself). The game master might filter the results based on difficulty level and whether or not the user is an adult.