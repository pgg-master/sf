<?php

namespace App\Repository;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\VarExporter\Exception\ClassNotFoundException;

final class BaseRepositoryFactory
{
    private $container;
    private $em;

    public function __construct(ContainerInterface $container, EntityManagerInterface $em)
    {
        $this->container = $container;
        $this->em = $em;
    }

    /**
     * @param string $entityName
     *
     * @return BaseRepository|object
     * @throws ClassNotFoundException
     */
    public function create(string $entityName): BaseRepository
    {
        if (!class_exists($entityName)) {
            throw new ClassNotFoundException($entityName . ' not found!');
        }

        $repositoryName = $entityName . 'Repository';

        if (!$this->container->has($repositoryName)) {
            $this->container->set($repositoryName, new BaseRepository($this->em, $entityName));
        }

        return $this->container->get($repositoryName);
    }
}
