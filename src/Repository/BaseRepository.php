<?php

namespace App\Repository;

use App\Constant\FilterConstant;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping;
use Doctrine\ORM\QueryBuilder;
use FOS\RestBundle\Request\ParamFetcherInterface;

final class BaseRepository extends EntityRepository
{
    public function __construct(EntityManagerInterface $em, string $class)
    {
        parent::__construct($em, new Mapping\ClassMetadata($class));
    }

    public function findByParamFetcher(ParamFetcherInterface $paramFetcher): array
    {
        return $this->getBaseBuilder($paramFetcher)->getQuery()->getResult();
    }

    public function getBaseBuilder(ParamFetcherInterface $paramFetcher): QueryBuilder
    {
        $builder = $this->createQueryBuilder('en');

        $page = $paramFetcher->get('page');
        if ($page !== null && $page > 0) {
            $limit = $paramFetcher->get('limit');
            $firstIndex = ($page - 1) * $limit;

            $builder
                ->setFirstResult($firstIndex)
                ->setMaxResults($limit)
            ;
        }

        return $builder;
    }

    public function countItems(): int
    {
        /** @noinspection PhpUnhandledExceptionInspection */
        return (int)$this
            ->createQueryBuilder('en')
            ->select('count(en.id)')
            ->getQuery()
            ->getSingleScalarResult()
        ;
    }

    public function findRandom(array $filters)
    {
        $builder = $this->createQueryBuilder('en')
            ->orderBy('RAND()')
            ->setMaxResults(1)
        ;

        if (isset($filters[FilterConstant::DIFFICULTY])) {
            $builder
                ->andWhere('en.difficulty in (:difficulties)')
                ->setParameter('difficulties', $filters[FilterConstant::DIFFICULTY])
            ;
        }

        if (isset($filters[FilterConstant::CHILDREN_FRIENDLY])) {
            $builder
                ->andWhere('en.childrenFriendly = :childrenFriendly')
                ->setParameter('childrenFriendly', $filters[FilterConstant::CHILDREN_FRIENDLY])
            ;
        }

        /** @noinspection PhpUnhandledExceptionInspection */
        return $builder
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
}
