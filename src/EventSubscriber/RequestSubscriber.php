<?php

namespace App\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class RequestSubscriber implements EventSubscriberInterface
{
    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::REQUEST => [
                ['onKernelRequest', 512],
            ],
        ];
    }

    public function onKernelRequest(RequestEvent $event): void
    {
        if ($event->getRequest()->getQueryString()) {
            $paramString = explode('?', $event->getRequest()->getRequestUri())[1];
            $params = [];
            foreach (explode('&', $paramString) as $rawParam) {
                [$key, $value] = explode('=', $rawParam);
                $params[$key][] = $value;
            }

            foreach ($params as $key => $value) {
                if (count($value) > 1) {
                    $event->getRequest()->query->set($key, $value);
                }
            }
        }
    }
}
