<?php

namespace App\Provider;

use Doctrine\Common\Collections\ArrayCollection;

class CollectionProvider
{
    public static function collection(array $items): ArrayCollection
    {
        return new ArrayCollection($items);
    }
}
