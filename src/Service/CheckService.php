<?php

namespace App\Service;

use App\Entity\Check;
use Doctrine\ORM\EntityManagerInterface;

class CheckService
{
    private $manager;

    public function __construct(EntityManagerInterface $manager)
    {
        $this->manager = $manager;
    }

    public function persist(Check $check): void
    {
        $this->manager->persist($check);
        $this->manager->flush();
    }
}
