<?php

namespace App\Service;

use App\Constant\FilterConstant;
use App\Entity\ResourceInterface;
use App\Repository\BaseRepositoryFactory;
use FOS\RestBundle\Request\ParamFetcherInterface;
use Symfony\Component\VarExporter\Exception\ClassNotFoundException;
use InvalidArgumentException;

abstract class AbstractResourceService
{
    protected $repository;
    protected $paramFetcher;

    /**
     * @param BaseRepositoryFactory $repositoryFactory
     * @param string                $resourceClass
     * @param ParamFetcherInterface $paramFetcher
     *
     * @throws ClassNotFoundException
     */
    public function __construct(
        BaseRepositoryFactory $repositoryFactory,
        string $resourceClass,
        ParamFetcherInterface $paramFetcher
    ) {
        $this->repository = $repositoryFactory->create($resourceClass);
        $this->paramFetcher = $paramFetcher;
    }

    public function findRandom(): ResourceInterface
    {
        $filters = $this->fetchFilters();
        $resource = $this->repository->findRandom($filters);
        if (!$resource) {
            throw new InvalidArgumentException('NO_RESULTS_MATCHING_FILTERS');
        }

        return $resource;
    }

    public function count(): int
    {
        return $this->repository->countItems();
    }

    private function fetchFilters(): array
    {
        $filters = [];

        if ($this->paramFetcher->get(FilterConstant::DIFFICULTY)) {
            $filters[FilterConstant::DIFFICULTY] = $this->paramFetcher->get(FilterConstant::DIFFICULTY);
        }
        if ($this->paramFetcher->get(FilterConstant::CHILDREN_FRIENDLY)) {
            $filters[FilterConstant::CHILDREN_FRIENDLY] = $this->paramFetcher->get(FilterConstant::CHILDREN_FRIENDLY);
        }

        return $filters;
    }
}
