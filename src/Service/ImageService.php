<?php

namespace App\Service;

use App\Entity\Image;
use App\Repository\BaseRepositoryFactory;
use FOS\RestBundle\Request\ParamFetcherInterface;

class ImageService extends AbstractResourceService
{
    public function __construct(BaseRepositoryFactory $repositoryFactory, ParamFetcherInterface $paramFetcher)
    {
        parent::__construct($repositoryFactory, Image::class, $paramFetcher);
    }
}
