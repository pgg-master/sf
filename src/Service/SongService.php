<?php

namespace App\Service;

use App\Entity\Song;
use App\Repository\BaseRepositoryFactory;
use FOS\RestBundle\Request\ParamFetcherInterface;

class SongService extends AbstractResourceService
{
    public function __construct(BaseRepositoryFactory $repositoryFactory, ParamFetcherInterface $paramFetcher)
    {
        parent::__construct($repositoryFactory, Song::class, $paramFetcher);
    }
}
