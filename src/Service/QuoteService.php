<?php

namespace App\Service;

use App\Entity\Quote;
use App\Repository\BaseRepositoryFactory;
use FOS\RestBundle\Request\ParamFetcherInterface;

class QuoteService extends AbstractResourceService
{
    public function __construct(BaseRepositoryFactory $repositoryFactory, ParamFetcherInterface $paramFetcher)
    {
        parent::__construct($repositoryFactory, Quote::class, $paramFetcher);
    }
}
