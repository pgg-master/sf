<?php

namespace App\Annotation\Parameter\Filter;

use App\Constant\FilterConstant;
use Doctrine\Common\Annotations\Annotation\Target;
use FOS\RestBundle\Controller\Annotations\ParamInterface;
use Swagger\Annotations\Parameter;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Annotation
 * @Target("METHOD")
 */
class Difficulty extends Parameter implements ParamInterface
{
    public function __construct(
        array $properties
    ) {
        parent::__construct($properties);

        $this->name = FilterConstant::DIFFICULTY;
        $this->in = 'query';
        $this->type = 'string';
        $this->description = 'Defines difficulties to include in query';
        $this->allowEmptyValue = false;
        $this->required = false;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getDefault(): string
    {
        return $this->default;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function getIncompatibilities(): array
    {
        return [];
    }

    public function getConstraints(): array
    {
        return [];
    }

    public function isStrict(): bool
    {
        return false;
    }

    public function getValue(Request $request, $default)
    {
        return $request->query->get($this->name, $default);
    }
}
