<?php

namespace App\Annotation\Parameter\Filter;

use App\Constant\FilterConstant;
use Doctrine\Common\Annotations\Annotation\Target;
use FOS\RestBundle\Controller\Annotations\ParamInterface;
use Swagger\Annotations\Parameter;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Annotation
 * @Target("METHOD")
 */
class ChildrenFriendly extends Parameter implements ParamInterface
{
    public function __construct(
        array $properties
    ) {
        parent::__construct($properties);

        $this->name = FilterConstant::CHILDREN_FRIENDLY;
        $this->in = 'query';
        $this->description = 'Defines whether or not to filter out NSFW resources';
        $this->type = 'boolean';
        $this->allowEmptyValue = false;
        $this->required = false;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getDefault(): string
    {
        return $this->default;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function getIncompatibilities(): array
    {
        return [];
    }

    public function getConstraints(): array
    {
        return [];
    }

    public function isStrict(): bool
    {
        return false;
    }

    public function getValue(Request $request, $default)
    {
        return (bool)$request->query->get($this->name, $default);
    }
}
