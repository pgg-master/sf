<?php

namespace App\Constant;

class FilterConstant
{
    public const DIFFICULTY = 'difficulty';
    public const CHILDREN_FRIENDLY = 'children-friendly';
}
