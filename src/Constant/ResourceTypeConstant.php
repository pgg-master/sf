<?php

namespace App\Constant;

class ResourceTypeConstant
{
    public const IMAGE = 'image';
    public const SONG = 'song';
    public const QUOTE = 'quote';

    public const ALL = [
        self::IMAGE,
        self::SONG,
        self::QUOTE
    ];
}
