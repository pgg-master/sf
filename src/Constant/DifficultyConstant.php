<?php

namespace App\Constant;

class DifficultyConstant
{
    public const EASY = 'EASY';
    public const MEDIUM = 'MEDIUM';
    public const HARD = 'HARD';

    public const ALL = [
        self::EASY,
        self::MEDIUM,
        self::HARD
    ];
}
