<?php

namespace App\Controller;

use App\Annotation\Parameter;
use App\Entity\Check;
use App\Service\CheckService;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcherInterface;
use Nelmio\ApiDocBundle\Annotation\Model;
use Symfony\Component\HttpFoundation\Response;
use App\Annotation\Response as HTTP;
use Swagger\Annotations as SWG;

class CheckController extends BaseController
{
    /**
     * @Rest\Post(
     *     path="/checks",
     *     name="create_check"
     * )
     * @SWG\Post(
     *     tags={"Check"},
     *     summary="CREATE CHECK",
     *     responses={
     *         @HTTP\Error400(),
     *         @SWG\Response(
     *             response="201",
     *             description="Created",
     *             @SWG\Schema(ref=@Model(type=Check::class, groups={"base", Check::CREATE_RESPONSE}))
     *         )
     *     }
     * )
     *
     * @Parameter\Instance(
     *     name="Check",
     *     @SWG\Schema(ref=@Model(type=Check::class, groups={Check::CREATE}))
     * )
     *
     * @param ParamFetcherInterface $paramFetcher
     * @param CheckService          $service
     *
     * @return Response
     */
    public function create(
        ParamFetcherInterface $paramFetcher,
        CheckService $service
    ): Response {
        /** @var Check $check */
        $check = $this->deserialize(
            $paramFetcher->get('Check'),
            Check::class,
            Check::CREATE
        );

        $this->validate($check, Check::CREATE);
        $service->persist($check);

        return $this->createObjectView($check, 201, Check::CREATE_RESPONSE);
    }
}
