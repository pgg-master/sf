<?php

namespace App\Controller;

use App\Annotation\Parameter;
use App\Service\QuoteService;
use FOS\RestBundle\Controller\Annotations as Rest;
use Nelmio\ApiDocBundle\Annotation\Model;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Response;
use Swagger\Annotations as SWG;
use App\Entity\AbstractResource;
use App\Entity\Quote;
use Exception;
use App\Annotation\Response as HTTP;

class QuoteController extends BaseController
{
    /**
     * @Rest\Get(
     *     path="/quotes/random",
     *     name="show_random_quote"
     * )
     * @SWG\Get(
     *     tags={"Quote"},
     *     summary="SHOW RANDOM QUOTE",
     *     responses={
     *         @SWG\Response(
     *             response="200",
     *             description="OK",
     *             @SWG\Schema(ref=@Model(type=Quote::class, groups={"base", AbstractResource::SHOW}))
     *         )
     *     }
     * )
     *
     * @Parameter\Group(enum={
     *     AbstractResource::SHOW
     * })
     * @Parameter\Filter\ChildrenFriendly()
     * @Parameter\Filter\Difficulty()
     *
     * @param QuoteService $service
     *
     * @return Response
     * @throws Exception
     */
    public function showRandom(
        QuoteService $service
    ): Response {
        $quote = $service->findRandom();

        return $this->createObjectView($quote);
    }

    /**
     * @Rest\Get(
     *     path="/quotes/{quoteId}",
     *     name="show_quote",
     *     requirements={"quoteId"="\d+"}
     * )
     * @SWG\Get(
     *     tags={"Quote"},
     *     summary="SHOW QUOTE",
     *     responses={
     *         @HTTP\Error404(),
     *         @SWG\Response(
     *             response="200",
     *             description="OK",
     *             @SWG\Schema(ref=@Model(type=Quote::class, groups={"base", AbstractResource::SHOW_DETAILS}))
     *         )
     *     }
     * )
     *
     * @Parameter\Group(enum={
     *     AbstractResource::SHOW,
     *     AbstractResource::SHOW_ANSWER,
     *     AbstractResource::SHOW_DETAILS
     * })
     *
     * @ParamConverter("quote", options={"id" = "quoteId"})
     * @param Quote $quote
     *
     * @return Response
     */
    public function show(
        Quote $quote
    ): Response {
        return $this->createObjectView($quote);
    }
}
