<?php

namespace App\Controller;

use App\Annotation\Parameter;
use App\Service\ImageService;
use FOS\RestBundle\Controller\Annotations as Rest;
use Nelmio\ApiDocBundle\Annotation\Model;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Response;
use Swagger\Annotations as SWG;
use App\Entity\AbstractResource;
use App\Entity\Image;
use Exception;
use App\Annotation\Response as HTTP;

class ImageController extends BaseController
{
    /**
     * @Rest\Get(
     *     path="/images/random",
     *     name="show_random_image"
     * )
     * @SWG\Get(
     *     tags={"Image"},
     *     summary="SHOW RANDOM IMAGE",
     *     responses={
     *         @SWG\Response(
     *             response="200",
     *             description="OK",
     *             @SWG\Schema(ref=@Model(type=Image::class, groups={"base", AbstractResource::SHOW}))
     *         )
     *     }
     * )
     *
     * @Parameter\Group(enum={
     *     AbstractResource::SHOW
     * })
     *
     * @param ImageService $service
     *
     * @return Response
     * @throws Exception
     */
    public function showRandom(
        ImageService $service
    ): Response {
        $image = $service->findRandom();

        return $this->createObjectView($image);
    }

    /**
     * @Rest\Get(
     *     path="/images/{imageId}",
     *     name="show_image",
     *     requirements={"imageId"="\d+"}
     * )
     * @SWG\Get(
     *     tags={"Image"},
     *     summary="SHOW IMAGE",
     *     responses={
     *         @HTTP\Error404(),
     *         @SWG\Response(
     *             response="200",
     *             description="OK",
     *             @SWG\Schema(ref=@Model(type=Image::class, groups={"base", AbstractResource::SHOW_DETAILS}))
     *         )
     *     }
     * )
     *
     * @Parameter\Group(enum={
     *     AbstractResource::SHOW,
     *     AbstractResource::SHOW_ANSWER,
     *     AbstractResource::SHOW_DETAILS
     * })
     *
     * @ParamConverter("image", options={"id" = "imageId"})
     * @param Image $image
     *
     * @return Response
     */
    public function show(
        Image $image
    ): Response {
        return $this->createObjectView($image);
    }
}
