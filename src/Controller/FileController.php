<?php

namespace App\Controller;

use App\Annotation\Parameter;
use App\Entity\File;
use App\Service\ImageService;
use FOS\RestBundle\Controller\Annotations as Rest;
use Nelmio\ApiDocBundle\Annotation\Model;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Response;
use Swagger\Annotations as SWG;
use App\Entity\AbstractResource;
use App\Entity\Image;
use Exception;
use App\Annotation\Response as HTTP;

class FileController extends BaseController
{
    /**
     * @Rest\Get(
     *     path="/files/{fileId}",
     *     name="show_file",
     *     requirements={"fileId"="\d+"}
     * )
     * @SWG\Get(
     *     tags={"File"},
     *     summary="SHOW FILE",
     *     responses={
     *         @HTTP\Error404(),
     *         @SWG\Response(
     *             response="200",
     *             description="OK",
     *             @SWG\Schema(ref=@Model(type=File::class, groups={"base", File::SHOW}))
     *         )
     *     }
     * )
     *
     * @Parameter\Group(enum={
     *     File::SHOW
     * })
     *
     * @ParamConverter("file", options={"id" = "fileId"})
     * @param File $file
     *
     * @return Response
     */
    public function show(
        File $file
    ): Response {
        return $this->createObjectView($file);
    }
}
