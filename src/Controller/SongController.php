<?php

namespace App\Controller;

use App\Annotation\Parameter;
use App\Entity\Song;
use App\Service\SongService;
use FOS\RestBundle\Controller\Annotations as Rest;
use Nelmio\ApiDocBundle\Annotation\Model;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Response;
use Swagger\Annotations as SWG;
use App\Entity\AbstractResource;
use Exception;
use App\Annotation\Response as HTTP;

class SongController extends BaseController
{
    /**
     * @Rest\Get(
     *     path="/songs/random",
     *     name="show_random_song"
     * )
     * @SWG\Get(
     *     tags={"Song"},
     *     summary="SHOW RANDOM SONG",
     *     responses={
     *         @SWG\Response(
     *             response="200",
     *             description="OK",
     *             @SWG\Schema(ref=@Model(type=Song::class, groups={"base", AbstractResource::SHOW}))
     *         )
     *     }
     * )
     *
     * @Parameter\Group(enum={
     *     AbstractResource::SHOW
     * })
     *
     * @param SongService $service
     *
     * @return Response
     * @throws Exception
     */
    public function showRandom(
        SongService $service
    ): Response {
        $song = $service->findRandom();

        return $this->createObjectView($song);
    }

    /**
     * @Rest\Get(
     *     path="/songs/{songId}",
     *     name="show_song",
     *     requirements={"songId"="\d+"}
     * )
     * @SWG\Get(
     *     tags={"Song"},
     *     summary="SHOW SONG",
     *     responses={
     *         @HTTP\Error404(),
     *         @SWG\Response(
     *             response="200",
     *             description="OK",
     *             @SWG\Schema(ref=@Model(type=Song::class, groups={"base", AbstractResource::SHOW_DETAILS}))
     *         )
     *     }
     * )
     *
     * @Parameter\Group(enum={
     *     AbstractResource::SHOW,
     *     AbstractResource::SHOW_ANSWER,
     *     AbstractResource::SHOW_DETAILS
     * })
     *
     * @ParamConverter("song", options={"id" = "songId"})
     * @param Song $song
     *
     * @return Response
     */
    public function show(
        Song $song
    ): Response {
        return $this->createObjectView($song);
    }
}
