<?php

namespace App\Controller;

use App\Exception\BadRequestHttpException;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Request\ParamFetcherInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

abstract class BaseController extends AbstractFOSRestController
{
    private $validator;
    private $paramFetcher;

    public function __construct(
        ValidatorInterface $validator,
        ParamFetcherInterface $paramFetcher
    ) {
        $this->validator = $validator;
        $this->paramFetcher = $paramFetcher;
    }

    protected function createObjectView(
        $object,
        int $statusCode = 200,
        string $group = null,
        bool $timestamped = false
    ): Response {
        $view = $this->view($object, $statusCode);
        $view->getContext()
            ->setGroups(['base'])
            ->addGroup($group ?? $this->paramFetcher->get('group'))
        ;

        if ($timestamped) {
            $view->getContext()->addGroup('timestamp');
        }

        return $this->handleView($view);
    }

    protected function createView($data = null, int $statusCode = 204): Response
    {
        $view = $this->view($data, $statusCode);

        return $this->handleView($view);
    }

    protected function createListView(
        array $objects,
        int $totalCount,
        int $limit,
        int $statusCode = 200,
        string $group = null
    ): Response {
        $view = $this->view(
            [
                'meta' => [
                    'total_count' => $totalCount,
                    'page_count' => ceil($totalCount/$limit)
                ],
                'data' => $objects
            ],
            $statusCode
        );
        $view->getContext()->setGroups(['base', $group ?? $this->paramFetcher->get('group')]);

        return $this->handleView($view);
    }

    /**
     * @param string        $data
     * @param string        $class
     * @param string        $group
     * @param object|null   $possessor  Instance to populate
     *
     * @return object
     */
    protected function deserialize(
        string $data,
        string $class,
        string $group,
        $possessor = null
    ) {
        $options = ['groups' => [$group]];
        if ($possessor) {
            $options['object_to_populate'] = $possessor;
        }

        return $this->getSerializer()->deserialize(
            $data,
            $class,
            'json',
            $options
        );
    }

    protected function validate($instance, string $group): void
    {
        $violations = $this->validator->validate($instance, null, $group);
        if ($violations->count() > 0) {
            throw new BadRequestHttpException($violations);
        }
    }

    private function getSerializer(): SerializerInterface
    {
        return $this->get('serializer');
    }
}
