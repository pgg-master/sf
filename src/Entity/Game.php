<?php

namespace App\Entity;

use App\Entity\Component\BaseEntity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Swagger\Annotations\Property;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 */
class Game extends BaseEntity
{
    /**
     * @var string
     *
     * @ORM\Column()
     *
     * @Assert\NotBlank(
     *     allowNull=false,
     *     message="GAME_NAME_CANNOT_BE_EMPTY",
     *     groups={""}
     * )
     *
     * @Property(
     *     type="string",
     *     example="Gothic II"
     * )
     * @Groups({
     *     AbstractResource::SHOW_ANSWER,
     *     AbstractResource::SHOW_DETAILS
     * })
     */
    private $name;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     *
     * @Assert\NotBlank(
     *     allowNull=false,
     *     message="GAME_RELEASE_YEAR_CANNOT_BE_EMPTY",
     *     groups={""}
     * )
     *
     * @Property(
     *     type="int",
     *     example=2002
     * )
     * @Groups({
     *     AbstractResource::SHOW_DETAILS
     * })
     */
    private $releaseYear;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="Platform")
     * @ORM\JoinTable(name="games_platforms",
     *      joinColumns={@ORM\JoinColumn(name="game_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="platform_id", referencedColumnName="id")}
     * )
     *
     * @Groups({
     *     AbstractResource::SHOW_DETAILS
     * })
     */
    private $platforms;

    public function __construct()
    {
        $this->platforms = new ArrayCollection();
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): Game
    {
        $this->name = $name;
        return $this;
    }

    public function getReleaseYear(): int
    {
        return $this->releaseYear;
    }

    public function setReleaseYear(int $year): Game
    {
        $this->releaseYear = $year;
        return $this;
    }

    public function getPlatforms(): Collection
    {
        return $this->platforms;
    }

    public function setPlatforms(ArrayCollection $platforms): Game
    {
        $this->platforms = $platforms;
        return $this;
    }
}
