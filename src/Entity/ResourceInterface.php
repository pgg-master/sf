<?php

namespace App\Entity;

interface ResourceInterface
{
    public function getGame(): Game;
    public function getDifficulty(): string;
    public function isChildrenFriendly(): bool;
}
