<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Swagger\Annotations\Property;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 */
class Quote extends AbstractResource
{
    /**
     * @var string
     *
     * @ORM\Column(type="text")
     *
     * @Assert\NotBlank(
     *     allowNull=false,
     *     message="QUOTE_CONTENT_CANNOT_BE_EMPTY",
     *     groups={""}
     * )
     *
     * @Property(
     *     type="string",
     *     example="Powiedz no, czy to coś z przodu twojej głowy to twarz, czy dupa?"
     * )
     * @Groups({
     *     AbstractResource::SHOW,
     *     AbstractResource::SHOW_DETAILS
     * })
     */
    private $content;

    /**
     * @var string
     *
     * @ORM\Column(nullable=true)
     *
     * @Property(
     *     type="string",
     *     example="Bezimienny"
     * )
     * @Groups({
     *     AbstractResource::SHOW_ANSWER,
     *     AbstractResource::SHOW_DETAILS
     * })
     */
    private $speaker;

    public function getContent(): string
    {
        return $this->content;
    }

    public function setContent(string $content): Quote
    {
        $this->content = $content;
        return $this;
    }

    public function getSpeaker(): ?string
    {
        return $this->speaker;
    }

    public function setSpeaker(?string $speaker): Quote
    {
        $this->speaker = $speaker;
        return $this;
    }
}
