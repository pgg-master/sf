<?php

namespace App\Entity;

use App\Entity\Component\BaseEntity;
use Doctrine\ORM\Mapping as ORM;
use Swagger\Annotations\Property;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 */
class Platform extends BaseEntity
{
    /**
     * @var string
     *
     * @ORM\Column()
     *
     * @Assert\NotBlank(
     *     allowNull=false,
     *     message="PLATFORM_NAME_CANNOT_BE_EMPTY",
     *     groups={""}
     * )
     *
     * @Property(
     *     type="string",
     *     example="PlayStation 3"
     * )
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column()
     *
     * @Assert\NotBlank(
     *     allowNull=false,
     *     message="PLATFORM_ABBR_CANNOT_BE_EMPTY",
     *     groups={""}
     * )
     *
     * @Property(
     *     type="string",
     *     example="PS3"
     * )
     * @Groups({
     *     AbstractResource::SHOW_DETAILS
     * })
     */
    private $abbr;

    /**
     * @var string
     *
     * @ORM\Column()
     *
     * @Assert\NotBlank(
     *     allowNull=false,
     *     message="PLATFORM_COLOR_CANNOT_BE_EMPTY",
     *     groups={""}
     * )
     *
     * @Property(
     *     type="string",
     *     example="#5D2161"
     * )
     * @Groups({
     *     AbstractResource::SHOW_DETAILS
     * })
     */
    private $color;

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): Platform
    {
        $this->name = $name;
        return $this;
    }

    public function getAbbr(): string
    {
        return $this->abbr;
    }

    public function setAbbr(string $abbr): Platform
    {
        $this->abbr = $abbr;
        return $this;
    }

    public function getColor(): string
    {
        return $this->color;
    }

    public function setColor(string $color): Platform
    {
        $this->color = $color;
        return $this;
    }
}
