<?php

namespace App\Entity;

use App\Entity\Component\BaseEntity;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 */
class File extends BaseEntity
{
    public const SHOW = 'show-file';

    /**
     * @var string
     *
     * @ORM\Column()
     *
     * @Assert\NotBlank(
     *     allowNull=false,
     *     message="FILENAME_CANNOT_BE_EMPTY",
     *     groups={""}
     * )
     */
    private $filename;

    /**
     * @var string
     *
     * @ORM\Column()
     *
     * @Assert\NotBlank(
     *     allowNull=false,
     *     message="EXTENSION_CANNOT_BE_EMPTY",
     *     groups={""}
     * )
     */
    private $extension;

    /**
     * @var string
     *
     * @ORM\Column()
     *
     * @Assert\NotBlank(
     *     allowNull=false,
     *     message="MIMETYPE_CANNOT_BE_EMPTY",
     *     groups={""}
     * )
     */
    private $mimetype;

    /**
     * @var string
     *
     * @ORM\Column(type="text")
     *
     * @Assert\NotBlank(
     *     allowNull=false,
     *     message="BASE64_CANNOT_BE_EMPTY"
     * )
     */
    private $base64;

    public function getFilename(): string
    {
        return $this->filename;
    }

    public function setFilename(string $filename): File
    {
        $this->filename = $filename;
        return $this;
    }

    public function getExtension(): string
    {
        return $this->extension;
    }

    public function setExtension(string $extension): File
    {
        $this->extension = $extension;
        return $this;
    }

    public function getMimetype(): string
    {
        return $this->mimetype;
    }

    public function setMimetype(string $mimetype): File
    {
        $this->mimetype = $mimetype;
        return $this;
    }

    public function getBase64(): string
    {
        return $this->base64;
    }

    public function setBase64(string $base64): File
    {
        $this->base64 = $base64;
        return $this;
    }

    /**
     * @SerializedName("src")
     *
     * @Groups({
     *     File::SHOW
     * })
     *
     * @return string
     */
    public function getSrc(): string
    {
        return sprintf(
            'data:%s;base64,%s',
            $this->mimetype,
            $this->base64
        );
    }
}
