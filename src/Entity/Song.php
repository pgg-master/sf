<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 */
class Song extends AbstractResource
{
    /**
     * @var File
     *
     * @ORM\OneToOne(targetEntity="File")
     * @ORM\JoinColumn(name="file_id", referencedColumnName="id")
     *
     * @Assert\NotNull(
     *     message="IMAGE_FILE_CANNOT_BE_EMPTY",
     *     groups={""}
     * )
     *
     * @Groups({
     *     AbstractResource::SHOW,
     *     AbstractResource::SHOW_DETAILS
     * })
     */
    private $file;

    /**
     * @var string
     *
     * @ORM\Column(nullable=true)
     *
     * @Groups({
     *     AbstractResource::SHOW_ANSWER,
     *     AbstractResource::SHOW_DETAILS
     * })
     */
    private $song;

    public function getFile(): File
    {
        return $this->file;
    }

    public function setFile(File $file): Song
    {
        $this->file = $file;
        return $this;
    }

    public function getSong(): ?string
    {
        return $this->song;
    }

    public function setSong(?string $song): Song
    {
        $this->song = $song;
        return $this;
    }
}
