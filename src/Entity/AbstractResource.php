<?php

namespace App\Entity;

use App\Entity\Component\BaseEntity;
use Doctrine\ORM\Mapping as ORM;
use Swagger\Annotations\Property;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use App\Constant\DifficultyConstant;

/**
 * @ORM\MappedSuperclass()
 */
abstract class AbstractResource extends BaseEntity implements ResourceInterface
{
    public const SHOW = 'show-resource';
    public const SHOW_ANSWER = 'show-resource-answer';
    public const SHOW_DETAILS = 'show-resource-details';

    /**
     * @var Game
     *
     * @ORM\ManyToOne(targetEntity="Game")
     * @ORM\JoinColumn(name="game_id", referencedColumnName="id")
     *
     * @Assert\NotNull(
     *     message="GAME_CANNOT_BE_EMPTY",
     *     groups={""}
     * )
     *
     * @Groups({
     *     AbstractResource::SHOW_ANSWER,
     *     AbstractResource::SHOW_DETAILS
     * })
     */
    private $game;

    /**
     * @var string
     *
     * @ORM\Column()
     *
     * @Assert\Choice(
     *     choices=DifficultyConstant::ALL,
     *     message="INVALID_DIFFICULTY",
     *     groups={""}
     * )
     *
     * @Property(
     *     type="string",
     *     example="HARD"
     * )
     */
    private $difficulty;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean")
     *
     * @Assert\NotNull(
     *     message="CHILDREN_FRIENDLY_NEEDS_TO_BE_SET",
     *     groups={""}
     * )
     *
     * @Property(
     *     type="string",
     *     example=false
     * )
     * @Groups({
     *     AbstractResource::SHOW_DETAILS
     * })
     */
    private $childrenFriendly;

    public function getGame(): Game
    {
        return $this->game;
    }

    public function setGame(Game $game): ResourceInterface
    {
        $this->game = $game;

        return $this;
    }

    public function getDifficulty(): string
    {
        return $this->difficulty;
    }

    public function setDifficulty(string $difficulty): ResourceInterface
    {
        $this->difficulty = $difficulty;

        return $this;
    }

    public function isChildrenFriendly(): bool
    {
        return $this->childrenFriendly;
    }

    public function setChildrenFriendly(bool $childrenFriendly): ResourceInterface
    {
        $this->childrenFriendly = $childrenFriendly;
        return $this;
    }
}
