<?php

namespace App\Entity;

use App\Constant\ResourceTypeConstant;
use App\Entity\Component\BaseEntity;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="check_data")
 */
class Check extends BaseEntity
{
    public const CREATE = 'create-check';
    public const CREATE_RESPONSE = 'create-check-response';

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     *
     * @Assert\NotNull(
     *     message="RESOURCE_ID_CANNOT_BE_NULL",
     *     groups={
     *         Check::CREATE
     *     }
     * )
     * @Assert\Type(
     *     type="integer",
     *     message="RESOURCE_ID_MUST_BE_OF_TYPE_INTEGER",
     *     groups={
     *         Check::CREATE
     *     }
     * )
     *
     * @Groups({
     *     Check::CREATE,
     *     Check::CREATE_RESPONSE
     * })
     */
    private $resourceId;

    /**
     * @var string
     *
     * @ORM\Column()
     *
     * @Assert\NotBlank(
     *     allowNull=false,
     *     message="RESOURCE_TYPE_CANNOT_BE_EMPTY",
     *     groups={
     *         Check::CREATE
     *     }
     * )
     * @Assert\Choice(
     *     choices=ResourceTypeConstant::ALL,
     *     message="INVALID_RESOURCE_TYPE",
     *     groups={
     *         Check::CREATE
     *     }
     * )
     *
     * @Groups({
     *     Check::CREATE,
     *     Check::CREATE_RESPONSE
     * })
     */
    private $resourceType;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean")
     *
     * @Assert\NotBlank(
     *     allowNull=false,
     *     message="GUESSED_CANNOT_BE_EMPTY",
     *     groups={
     *         Check::CREATE
     *     }
     * )
     * @Assert\Type(
     *     type="boolean",
     *     message="GUESSED_MUST_BE_OF_TYPE_BOOLEAN",
     *     groups={
     *         Check::CREATE
     *     }
     * )
     *
     * @Groups({
     *     Check::CREATE,
     *     Check::CREATE_RESPONSE
     * })
     */
    private $guessed;

    public function getResourceId(): int
    {
        return $this->resourceId;
    }

    public function setResourceId(int $id): Check
    {
        $this->resourceId = $id;
        return $this;
    }

    public function getResourceType(): string
    {
        return $this->resourceType;
    }

    public function setResourceType(string $type): Check
    {
        $this->resourceType = $type;
        return $this;
    }

    public function isGuessed(): bool
    {
        return $this->guessed;
    }

    public function setGuessed(bool $guessed): Check
    {
        $this->guessed = $guessed;
        return $this;
    }
}
