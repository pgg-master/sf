<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 */
class Image extends AbstractResource
{
    /**
     * @var File
     *
     * @ORM\OneToOne(targetEntity="File")
     * @ORM\JoinColumn(name="file_id", referencedColumnName="id")
     *
     * @Assert\NotNull(
     *     message="IMAGE_FILE_CANNOT_BE_EMPTY",
     *     groups={""}
     * )
     *
     * @Groups({
     *     AbstractResource::SHOW,
     *     AbstractResource::SHOW_DETAILS
     * })
     */
    private $file;

    /**
     * @var File
     *
     * @ORM\OneToOne(targetEntity="File")
     * @ORM\JoinColumn(name="answer_file_id", referencedColumnName="id")
     *
     * @Assert\NotNull(
     *     message="ANSWER_IMAGE_FILE_CANNOT_BE_EMPTY",
     *     groups={""}
     * )
     *
     * @Groups({
     *     AbstractResource::SHOW,
     *     AbstractResource::SHOW_DETAILS
     * })
     */
    private $answerFile;

    /**
     * @var string
     *
     * @ORM\Column(nullable=true)
     *
     * @Groups({
     *     AbstractResource::SHOW_ANSWER,
     *     AbstractResource::SHOW_DETAILS
     * })
     */
    private $context;

    public function getFile(): File
    {
        return $this->file;
    }

    public function setFile(File $file): Image
    {
        $this->file = $file;
        return $this;
    }

    public function getAnswerFile(): File
    {
        return $this->answerFile;
    }

    public function setAnswerFile(File $answerFile): Image
    {
        $this->answerFile = $answerFile;
        return $this;
    }

    public function getContext(): ?string
    {
        return $this->context;
    }

    public function setContext(?string $context): Image
    {
        $this->context = $context;
        return $this;
    }
}
